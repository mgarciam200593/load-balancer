# Módulo AWS Load Balancer

Módulo de Terraform para crear recursos de Load Balancer en AWS

## Uso

Para utilzar el módulo de Load Balancer, se necesita primero crear la infraestructura de VPC y Security Group (módulos VPC y Security Group), ya que se necesitan recursos de dichos módulos, para este ejemplo se asume que la infraestructura ya ha sido desplega.
Para utilizar los recursos de VPC y Security Group, se hará uso del recurso data "terraform_remote_state"

### Network Load Balancer

```hcl

data "terraform_remote_state" "vpc" {
  backend = "s3"

  config = {
    bucket = "bda-tfstate-backend"
    key    = "dev/vpc/terraform.tfstate"
    region = "us-east-1"
  }
}

module "nlb" {
  source  = "modules/load-balancer"

  name               = "testing-nlb"
  load_balancer_type = "network"
  internal           = false
  subnets            = data.terraform_remote_state.vpc.outputs.private_subnet_ids

  tags = {
    ManagedBy   = "Terraform"
    Environment = "Development"
    Owner       = "Laboratorio Digital BDA"
  }
}
```

### Application Load Balancer

```hcl

data "terraform_remote_state" "vpc" {
  backend = "s3"

  config = {
    bucket = "bda-tfstate-backend"
    key    = "dev/vpc/terraform.tfstate"
    region = "us-east-1"
  }
}

data "terraform_remote_state" "sg" {
  backend = "s3"

  config = {
    bucket = "bda-tfstate-backend"
    key    = "dev/sg/terraform.tfstate"
    region = "us-east-1"
  }
}

module "alb" {
  source  = "modules/load-balancer"

  name               = "testing-alb"
  load_balancer_type = "application"
  internal           = true
  security_groups    = data.terraform_remote_state.sg.security_group_ids
  subnets            = data.terraform_remote_state.vpc.outputs.private_subnet_ids

  tags = {
    ManagedBy   = "Terraform"
    Environment = "Development"
    Owner       = "Laboratorio Digital BDA"
  }
}
```

## Requerimientos

| Nombre | Versión |
|------|---------|
| <a name="requirement_terraform"></a> [terraform](#requirement\_terraform) | >= 1.0 |
| <a name="requirement_aws"></a> [aws](#requirement\_aws) | >= 5.0 |

## Proveedores

| Nombre | Versión |
|------|---------|
| <a name="provider_aws"></a> [aws](#provider\_aws) | >= 5.0 |


## Recursos

| Nombre | Tipo |
|------|------|
| [aws_lb.this](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/lb) | resource |

## Inputs

| Nombre | Descripción | Tipo | Por Defecto | Requerido |
|------|-------------|------|---------|:--------:|
| <a name="input_name"></a> [name](#input_name) | Nombre del Balanceador | `string` | `null` | Sí |
| <a name="input_load_balancer_type"></a> [load_balancer_type](#input_load_balancer_type) | Tipo de Balanceador (Aplicación o Red) | `string` | `"aplicación"` | Sí |
| <a name="input_internal"></a> [internal](#input_internal) | Balanceador interno o internet-facing | `bool` | `false` | Sí |
| <a name="input_security_groups"></a> [security_groups](#input_security_groups) | IDs de los Security Groups para el Balanceador (aplica sólo para ALB) | `list(string)` | `[]` | no |
| <a name="input_subnets"></a> [subnets](#input_subnets) | IDs de Subnets que se asociarán al Balanceador | `list(string)` | `[]` | no |
| <a name="input_lb_tags"></a> [lb_tags](#input_lb_tags) | Tags para el Balanceador | `map(string)` | `{}` | no |
| <a name="input_tags"></a> [tags](#input_tags) | Tags los recursos del módulo | `map(string)` | `{}` | no |


## Outputs

| Nombre | Descripción |
|------|-------------|
| <a name="output_load_balancer_id"></a> [load_balancer_id](#output_load_balancer_id) | Load Balancer ID |
| <a name="output_load_balancer_arn"></a> [load_balancer_arn](#output_load_balancer_arn) | Load Balancer Arn |
| <a name="output_load_balancer_dns_name"></a> [load_balancer_dns_name](#output_load_balancer_dns_name) | Load Balancer DNS Name |
| <a name="output_load_balancer_zone_id"></a> [load_balancer_zone_id](#output_load_balancer_zone_id) | Load Balancer Zone ID |