variable "name" {
  type        = string
  description = "Name of Load Balancer"
  default     = null
}

variable "load_balancer_type" {
  type        = string
  description = "Type of Load Balancer to create (application or network)"
  default     = "application"
}

variable "internal" {
  type        = bool
  description = "Determining if Load Balancer is internal or externally facing"
  default     = false
}

variable "security_groups" {
  type        = list(string)
  description = "Security Groups IDs to attach to the Load Balancer, only applicable to Application LB"
  default     = []
}

variable "subnets" {
  type        = list(string)
  description = "Subnets IDs to associate with Load Balancer"
  default     = []
}

variable "tags" {
  type        = map(string)
  description = "Tags for all Load Balancer Module resources"
  default     = {}
}

variable "lb_tags" {
  type        = map(string)
  description = "Tags for Load Balancer resources"
  default     = {}
}
