# Load Balancer
###########################################################################
output "load_balancer_id" {
  value = aws_lb.this.id
  description = "ID of Load Balancer"
}

output "load_balancer_arn" {
  value = aws_lb.this.arn
  description = "Arn of Load Balancer"
}

output "load_balancer_dns_name" {
  value = aws_lb.this.dns_name
  description = "DNS Name of Load Balancer"
}

output "load_balancer_zone_id" {
  value = aws_lb.this.zone_id
  description = "Zone ID of Load Balancer"
}
