# Módulo AWS Target Group

Módulo de Terraform para crear recursos de Target Group en AWS

## Uso

Para utilzar el módulo de Target Group, se necesita primero crear la infraestructura de VPC y Load Balancer (módulos VPC y Load Balancer), ya que se necesitan recursos de dichos módulos, para este ejemplo se asume que la infraestructura ya ha sido desplega.
Para utilizar los recursos de VPC y Load Balancer, se hará uso del recurso data "terraform_remote_state"

```hcl

data "terraform_remote_state" "vpc" {
  backend = "s3"

  config = {
    bucket = "bda-tfstate-backend"
    key    = "dev/vpc/terraform.tfstate"
    region = "us-east-1"
  }
}

data "terraform_remote_state" "lb" {
  backend = "s3"

  config = {
    bucket = "bda-tfstate-backend"
    key    = "dev/lb/terraform.tfstate"
    region = "us-east-1"
  }
}

module "tg" {
  source  = "modules/load-balancer"

  target_group = {
    name         = "testing-tg"
    port         = 80
    protocol     = "TCP"
    target_type  = "ip"
    health_check = {
      healthy_threshold   = 3
      unhealthy_threshold = 2
      interval            = 10
    }
  }
  vpc_id = data.terraform_remote_state.vpc.outputs.vpc_id
  listener = [
    {
      lb_arn   = data.terraform_remote_state.lb.outputs.load_balancer_arn
      port     = "80" 
      protocol = "TCP"
    }
  ]

  tags = {
    ManagedBy   = "Terraform"
    Environment = "Development"
    Owner       = "Laboratorio Digital BDA"
  }
}
```

## Requerimientos

| Nombre | Versión |
|------|---------|
| <a name="requirement_terraform"></a> [terraform](#requirement\_terraform) | >= 1.0 |
| <a name="requirement_aws"></a> [aws](#requirement\_aws) | >= 5.0 |

## Proveedores

| Nombre | Versión |
|------|---------|
| <a name="provider_aws"></a> [aws](#provider\_aws) | >= 5.0 |


## Recursos

| Nombre | Tipo |
|------|------|
| [aws_lb_target_group.this](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/lb_target_group) | resource |
| [aws_lb_listener.this](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/lb_listener) | resource |

## Inputs

| Nombre | Descripción | Tipo | Por Defecto | Requerido |
|------|-------------|------|---------|:--------:|
| <a name="input_target_group"></a> [target_group](#input_target_group) | Variable con los atributos necesario para crear Target Group | `object` | `{}` | Sí |
| <a name="input_target_group_name"></a> [target_group.name](#input_target_group_name) | Nombre del Target Group | `string` | `""` | Sí |
| <a name="input_target_group_port"></a> [target_group.port](#input_target_group_port) | Puerto del Target Group | `number` | `null` | Sí |
| <a name="input_target_group_protocol"></a> [target_group.protocol](#input_target_group_protocol) | Protocolo del Target Group (HTTP, HTTPS, TCP, etc) | `string` | `""` | Sí |
| <a name="input_target_group_protocol_version"></a> [target_group.protocol_version](#input_target_group_protocol_version) | Versión del protocolo, sólo aplicable cuando el protocolo es HTTP o HTTPS (GRPC, HTTP1 o HTTP2) | `string` | `null` | no |
| <a name="input_target_group_target_type"></a> [target_group.target_type](#input_target_group_target_type) | Tipo de target a registrar en Target Group. Las opciones son "instance", "ip", "lambda" (no soportado por NLB) y "alb" (no soportado por ALB) | `string` | `null` | no |
| <a name="input_target_group_proxy_protocol"></a> [target_group.proxy_protocol](#input_target_group_proxy_protocol) | Habilitar soporte de proxy protocol v2 en NLB | `bool` | `false` | no |
| <a name="input_target_group_lambda_multi_value_headers"></a> [target_group.lambda_multi_value_headers](#input_target_group_lambda_multi_value_headers) | Cuando los request y response headers entre el Balanceador y Lambda incluyen arrays de valores o strings. Sólo aplica cuando el tipo de target es igual a "lambda" | `bool` | `false` | no |
| <a name="input_target_group_preserve_client_ip"></a> [target_group.preserve_client_ip](#input_target_group_preserve_client_ip) | Habiliar el preservar la IP del cliente | `bool` | `false` | no |
| <a name="input_target_group_health_check"></a> [target_group.health_check](#input_target_group_health_check) | Variable con los atributos necesarios para configurar health check en Target Group | `object` | `{}` | Sí |
| <a name="input_target_group_health_check_healthy_threshold"></a> [target_group.health_check.healthy_threshold](#input_target_group_health_check_healthy_threshold) | Número de health checks exitosos de manera consecutiva requeridos para considerar un target como saludable | `number` | `null` | Sí |
| <a name="input_target_group_health_check_unhealthy_threshold"></a> [target_group.health_check.unhealthy_threshold](#input_target_group_health_check_unhealthy_threshold) | Número de health checks fallidos de manera consecutiva para considerar un target como no saludable | `number` | `null` | Sí |
| <a name="input_target_group_health_check_interval"></a> [target_group.health_check.interval](#input_target_group_health_check_interval) | Cantidad de tiempo en segundos entre health checks hacia un target individual (el rango es de 5 a 300)  | `number` | `null` | Sí |
| <a name="input_target_group_health_matcher"></a> [target_group.health_check.matcher](#input_target_group_health_matcher) | Códigos de respuesta a utilizar para revisar la respuesta exitosa del target | `string` | `null` | no |
| <a name="input_vpc_id"></a> [vpc_id](#input_vpc_id) | ID de la VPC donde se creará el Target Group | `string` | `""` | Sí |
| <a name="input_listener"></a> [listerner](#input_listener) | Variable con los atributos necesarios para crear Listeners | `list(object)` | `[]` | Sí |
| <a name="input_listener_lb_arn"></a> [listerner.lb_arn](#input_listener_lb_arn) | Arn del Balanceador para asociacer el Listener | `string` | `""` | Sí |
| <a name="input_listener_port"></a> [listerner.port](#input_listener_port) | Puerto del Listener | `string` | `""` | Sí |
| <a name="input_listener_protocol"></a> [listerner.protocol](#input_listener_protocol) | Protocolo del Listener | `string` | `""` | Sí |
| <a name="input_listener_certificate"></a> [listerner.certificate](#input_listener_certificate) | Certificado para el Listener (aplicable cuando el protocolo es HTTPS o TLS) | `string` | `null` | no |
| <a name="input_listener_alpn_policy"></a> [listerner.alpn_policy](#input_listener_alpn_policy) | Politica Application-Layer Protocol Negotiation, configurable cuando el protocolo es TLS. Los valores válidos son "HTTP1Only", "HTTP2Only", "HTTP2Optional", "HTTP2Preferred" y "None" | `string` | `null` | no |
| <a name="input_listener_tags"></a> [listerner.tags](#input_listener_tags) | Tags para los Listeners | `map(string)` | `{}` | no |
| <a name="input_tg_tags"></a> [tg_tags](#input_tg_tags) | Tags para Target Group | `map(string)` | `{}` | no |
| <a name="input_tags"></a> [tags](#input_tags) | Tags los recursos del módulo | `map(string)` | `{}` | no |


## Outputs

| Nombre | Descripción |
|------|-------------|
| <a name="output_target_group_name"></a> [target_group_name](#output_target_group_name) | Target Groupt Name |
| <a name="output_target_group_id"></a> [target_group_id](#output_target_group_id) | Target Group ID |
| <a name="output_target_group_arn"></a> [target_group_arn](#output_target_group_arn) | Target Group ARN |
| <a name="output_listener_id"></a> [listener_id](#output_listener_id) | Listener ID |
| <a name="output_listener_arn"></a> [listener_arn](#output_listener_arn) | Listener Arn |