# Target Group
###########################################################################
output "target_group_name" {
  value       = aws_lb_target_group.this.name
  description = "Name of Target Group"
}

output "target_group_id" {
  value       = aws_lb_target_group.this.id
  description = "ID of Target Group"
}

output "target_group_arn" {
  value       = aws_lb_target_group.this.arn
  description = "Arn of Target Group"
}

# Listener
###########################################################################
output "listener_id" {
  value       = aws_lb_listener.this[*].id
  description = "IDs of Listeners"
}

output "listener_arn" {
  value       = aws_lb_listener.this[*].arn
  description = "Arn of Listeners"
}