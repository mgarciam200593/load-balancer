variable "target_group" {
  type = object({
    name                       = string
    port                       = number
    protocol                   = string
    protocol_version           = optional(string)
    target_type                = optional(string)
    proxy_protocol             = optional(bool)
    lambda_multi_value_headers = optional(bool)
    preserve_client_ip         = optional(bool)
    health_check = object({
      healthy_threshold   = number
      unhealthy_threshold = number
      interval            = number
      protocol            = optional(string, "HTTP")
      path                = optional(string, "/openapi.json")
      matcher             = optional(string)
    })
  })
}

variable "vpc_id" {
  type        = string
  description = "VPC ID where Target Groups will be created"
}

variable "target_id" {
  type        = string
  description = "ALB ARN to attach to Target Group"
  default     = null
}

variable "listener" {
  type = list(object({
    lb_arn      = string
    port        = string
    protocol    = string
    certificate = optional(string, null)
    tags        = optional(map(string), {})
    alpn_policy = optional(string, null)
  }))
}

variable "tags" {
  type    = map(string)
  default = {}
}

variable "tg_tags" {
  type    = map(string)
  default = {}
}