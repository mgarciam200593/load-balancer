# Target Group Module

# Target Group
###########################################################################
resource "aws_lb_target_group" "this" {
  name_prefix                        = "tg-"
  vpc_id                             = var.vpc_id
  port                               = try(var.target_group.port, null)
  protocol                           = try(var.target_group.protocol, null)
  protocol_version                   = try(var.target_group.protocol_version, null)
  target_type                        = try(var.target_group.target_type, null)
  proxy_protocol_v2                  = try(var.target_group.proxy_protocol, false)
  lambda_multi_value_headers_enabled = try(var.target_group.lambda_multi_value_headers, false)
  preserve_client_ip                 = try(var.target_group.preserve_client_ip, false)
  ip_address_type                    = "ipv4"

  health_check {
    enabled             = var.target_group.target_type == "lambda" ? false : true
    healthy_threshold   = var.target_group.health_check.healthy_threshold
    unhealthy_threshold = var.target_group.health_check.unhealthy_threshold
    interval            = var.target_group.health_check.interval
    path                = var.target_group.health_check.path
    matcher             = try(var.target_group.health_check.matcher, null)
    protocol            = var.target_group.health_check.protocol
  }

  lifecycle {
    create_before_destroy = true
  }

  tags = merge(
    { Name = var.target_group.name },
    var.tags
  )
}

resource "aws_lb_target_group_attachment" "test" {
  count = var.target_group.target_type == "alb" ? 1 : 0

  target_group_arn = aws_lb_target_group.this.arn
  target_id        = var.target_id

  depends_on = [
    aws_lb_target_group.this
  ]
}

# Listener
###########################################################################
resource "aws_lb_listener" "this" {
  count = length(var.listener)

  load_balancer_arn = var.listener[count.index].lb_arn
  port              = var.listener[count.index].port
  protocol          = var.listener[count.index].protocol
  certificate_arn   = try(var.listener[count.index].certificate, null)
  alpn_policy       = try(var.listener[count.index].alpn_policy, null)

  default_action {
    type             = "forward"
    target_group_arn = aws_lb_target_group.this.arn
  }

  tags = merge(
    var.tags,
    var.listener[count.index].tags
  )

  depends_on = [
    aws_lb_target_group.this
  ]
}
