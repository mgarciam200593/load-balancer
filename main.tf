# Load Balancer Module


# Load Balancer
###########################################################################
resource "aws_lb" "this" {
  name               = var.name
  load_balancer_type = var.load_balancer_type
  internal           = var.internal
  security_groups    = var.load_balancer_type == "application" ? var.security_groups : null
  subnets            = var.subnets

  ip_address_type = "ipv4"

  tags = merge(
    { Name = var.name },
    var.tags,
    var.lb_tags
  )
}


